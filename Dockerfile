FROM metabase/metabase

VOLUME [ "/data" ]
ENTRYPOINT [ "/bin/bash", "-c"]

EXPOSE 12345:3000
EXPOSE 3000:3000

ENV MB_DB_TYPE=postgres
ENV MB_DB_DBNAME=metabase
ENV MB_DB_PORT=5432
ENV MB_DB_USER=admin
ENV MB_DB_PASS=rafaebar215
ENV MB_DB_HOST=my-database-host
