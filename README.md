# ms-metabase-infra



## Introdução

Este é um projeto pessoal no qual busco aprimorar minhas habilidades, ou seja, apenas para estudo.
Aqui faço um deploy do [metabase](https://www.metabase.com/) na AWS, usando um ECS como orquestrador de containers e o RDS como banco de dados.
Tudo isso é aplicando via terraform em uma pipeline do próprio gitlab

## Requisitos
- Uma conta AWS
- Terraform instalado na máquina local
- Suas credenciais da AWS configuradas em seu ambiente para uso do terraform. Existem muitas maneiras de fazer isso. Se você tiver o aws cli instalado, provavelmente poderá apenas executar o ```aws configure```


## Arquitetura

- 1 vpc na região especificada pelo usuário (vpc default)
- 2 sub-redes associadas a AZs especificadas  (subnets default)
- Grupo de segurança para o contêiner da metabase
- Grupo de segurança para o cluster de banco de dados postgres
- Nome de usuário do banco de dados e outros segredos especificado pelo usuário (definido em terraform.tfvars)
-Definição de tarefa (FARGATE) para a configuração do contêiner da metabase
- Serviço ECS para definição de tarefa de metabase


###  Uso

1. ```git clone```
2. Os devidos ajustes...
3. ```git add .``` 
4. ```git commit -m "..."```
5. ```git push```

e gitops em ação..

