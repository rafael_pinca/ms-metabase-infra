resource "aws_default_vpc" "default" {}

data "aws_subnets" "default" {}

resource "aws_lb_target_group" "mde_mb" {
  name                 = "metabase-tg"
  port                 = 3000
  protocol             = "HTTP"
  target_type          = "ip"
  vpc_id               = aws_default_vpc.default.id
  deregistration_delay = 30
}

resource "aws_lb" "mde_mb" {
  name               = "metabase-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = data.aws_subnets.default.ids
  security_groups    = [aws_security_group.ecs_security_group.id]

  tags = {
    Name = "elb-metabase"
  }
}

resource "aws_lb_listener" "mde_mb" {
  load_balancer_arn = aws_lb.mde_mb.arn
  port              = var.port_container
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.mde_mb.arn
  }
}
