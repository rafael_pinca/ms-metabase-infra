variable "region" {}
variable "porta" {}
variable "cluster_name" {}
variable "task_name" {}
variable "network_mode" {}
variable "cluster_service_name" {}
variable "launch_type" {}
variable "port_container" {}
variable "db_host" { description = "Endpoint do RDS" }
variable "db_port" { description = "Porta do endpoint RDS" }
variable "user" {}
variable "pass" {}
variable "cpu" {}
variable "mem" {}