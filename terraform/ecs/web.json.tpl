[
  {
    "name": "metabase",
    "image": "metabase/metabase:v0.37.8",
    "essential": true,
    "portMappings": [
      {
        "containerPort": ${port},
        "hostPort": ${port}
      }
    ],
  "environment" : [
    { "name": "JAVA_TOOL_OPTIONS", "value": "-Xmx4g"    },
    { "name": "MB_DB_TYPE",        "value": var.db_type },
    { "name": "MB_DB_NAME",        "value": var.db_name },
    { "name": "MB_DB_HOST",        "value": var.db_host },
    { "name": "MB_DB_PORT",        "value": var.db_port },
    { "name": "MB_DB_USER",        "value": var.db_usr  },
    { "name": "MB_DB_PASS",        "value": var.db_pwd  }
  ]
}])