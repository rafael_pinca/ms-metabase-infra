output "cluster_arn" {
  value = aws_ecs_cluster.my_cluster.arn
}

output "cluster_name" {
  value = aws_ecs_cluster.my_cluster.name
}

output "service_name" {
  value = aws_ecs_service.my_service.name
}

output "task" {
  value = aws_ecs_service.my_service.task_definition
}


output "task_name" {
  value = aws_ecs_task_definition.my_task.arn
}

output "sg_arn" {
  value = aws_security_group.ecs_security_group.arn
}

output "rendered_json" {
  value = data.template_file.web_ui_tpl.rendered
}
