
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = var.region 
}

resource "aws_ecs_cluster" "my_cluster" {
  name = var.cluster_name
}

resource "aws_ecs_task_definition" "my_task" {
  family                   = var.task_name
  network_mode             = var.network_mode
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.cpu
  memory                   = var.mem

###
# É possivel usar o arquivo randerizado ou descomentando as linhas abaixo,
# container_definitions = jsonencode([{...
###

container_definitions = data.template_file.web_ui_tpl.rendered
#     container_definitions = jsonencode([{
#       name  = "metabase",
#       image = "metabase/metabase:latest",
#       portMappings = [{
#         containerPort = 3000,
#         #hostPort      = 0,
#       }]
#       environment = [
#         { name = "MB_DB_TYPE", value = "postgres" },
#         { name = "MB_DB_DBNAME", value = "metabase" },
#         { name = "MB_DB_PORT", value = "5432" },
#         { name = "MB_DB_USER", value = var.user },
#         { name = "MB_DB_PASS", value = var.pass },
#         { name = "MB_DB_HOST", value = var.db_host },
#       ]
#     }])
 }

resource "aws_ecs_service" "my_service" {
  name            = var.cluster_service_name
  cluster         = aws_ecs_cluster.my_cluster.id
  task_definition = aws_ecs_task_definition.my_task.arn
  launch_type     = var.launch_type
  desired_count   = 1

  network_configuration {
    subnets          = slice(data.aws_subnets.default.ids, 0, 2)
    assign_public_ip = true
    security_groups  = [aws_security_group.ecs_security_group.id]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.mde_mb.arn
    container_name   = "metabase"
    container_port   = var.port_container
  }
}

data "template_file" "web_ui_tpl" {
  template = file("./web.json.tpl")

  vars = {
    port    = var.port_container
    db_usr  = var.user
    db_pwd  = var.pass
    db_host = var.db_host #aws_rds_cluster.pg.endpoint
    db_port = var.db_port #aws_rds_cluster.pg.port
    db_type = "postgres"
    db_name = "metabase"
  }
}