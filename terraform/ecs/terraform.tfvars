region               = "us-east-1"
porta                = "5432"
cluster_name         = "db-ms-metabase"
task_name            = "metabase"
network_mode         = "awsvpc"
cluster_service_name = "cluster-01"
launch_type          = "FARGATE"
cpu = "1024"
mem = "2048"


port_container       = 3000
