resource "aws_security_group" "ecs_security_group" {
  name        = "ecs_security_group"
  description = "Allow incoming traffic for ECS tasks"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}