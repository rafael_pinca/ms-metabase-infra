variable "region" {}
variable "cluster_identifier" {}
variable "engine" {}
variable "database_name" {}
variable "db_subnet_group_name" {}
variable "master_username" {}
variable "master_password" {}
variable "engine_mode" {}
variable "scaling_configuration" {type = map(string)}