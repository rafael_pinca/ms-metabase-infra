
output "arn" {
  value = aws_rds_cluster.pg.arn
} 

output "endpoint" {
  value = aws_rds_cluster.pg.endpoint
}

output "port" {
  value = aws_rds_cluster.pg.port
}