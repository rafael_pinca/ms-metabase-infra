region = "us-east-1"
cluster_identifier = "aurora-pg-mde-mb"
engine = "aurora-postgresql"
database_name = "metabase"
db_subnet_group_name = "default"
engine_mode = "serverless"
scaling_configuration = {
  "max_capacity" = "6"
  "min_capacity" = "2"
}
