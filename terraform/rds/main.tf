data "aws_availability_zones" "available" {}
terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.74.0"
    }
  }
}

provider "aws" {
  region = var.region

}

locals {
  name = "metabase"

  region      = "us-east-1"
  environment = "prod"
  #vpc_cidr    = "10.0.0.0/16"
  #azs         = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = {
    Name        = local.name
    environment = local.environment
    Repository  = "https://gitlab.com/rafael_pinca/ms-metabase-infra"
  }
}



resource "aws_rds_cluster" "pg" {
  cluster_identifier        = var.cluster_identifier
  engine                    = var.engine
  database_name             = var.database_name
  db_subnet_group_name      = var.db_subnet_group_name
  master_username           = var.master_username
  master_password           = var.master_password
  engine_mode               = var.engine_mode
  skip_final_snapshot       = true

  tags = local.tags

  scaling_configuration {
    max_capacity             = 6
    min_capacity             = 2
  }
}